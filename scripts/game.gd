extends Node

onready var vel = get_node('Velocity')
onready var acc = get_node('Acceleration')
onready var rot = get_node('Rotation')

onready var player = get_node('player')

func _ready():
	set_process(true)
	vel.set_pos(Vector2(100,100))
	rot.set_pos(Vector2(200,200))
	acc.set_pos(Vector2(300,300))
	pass

func _process(delta):
	vel.set_bbcode(str(player.vel))
	acc.set_bbcode(str(player.acc))
	rot.set_bbcode(str(player.rot))

