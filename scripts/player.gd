extends Area2D

export (int) var rotation_speed = 2.6
export (int) var thrust_speed   = 0500
export (int) var MAX_VELOCITY   = 400
export (int) var FRICTION       = 0.65

var screenSize = Vector2()
var pos = Vector2()
var vel = Vector2()
var acc = Vector2()
var rot = 0

func _ready():
	screenSize = get_viewport_rect().size
	pos = screenSize/2
	set_pos(pos)
	set_process(true)

func _process(delta):
	if Input.is_action_pressed("left"):
		rot += rotation_speed * delta
	elif Input.is_action_pressed("right"):
		rot -= rotation_speed * delta
	if Input.is_action_pressed("up"):
		acc = Vector2(thrust_speed, 0).rotated(rot)
	else: acc *= 0
	
	if pos.x >= screenSize.width:
		pos.x = screenSize.width
	if pos.y >= screenSize.height:
		pos.y = screenSize.height
	if pos.x <= 0:
		pos.x = 0
	if pos.y <= 0:
		pos.y = 0
	
	acc += vel * -FRICTION
	vel += acc * delta
	pos += vel * delta
	
	set_pos(pos)
	set_rot(rot - PI/2)

